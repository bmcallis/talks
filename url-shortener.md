# Creating a personal URL shortener

This information originally came from a live stream that Kent C. Dodds did.
[Make a SUPER simple personal URL shortener with Netlify](https://www.youtube.com/watch?v=HL6paXyx6hM)

It's extrememly easy to create your very own url shortener in just a few quick and easy steps. The only cost is for the domain name where you want your shortener to be.

1. Create a repo in [gitlab](https://gitlab.com), [github](https://github.com), or [bitbucket](https://bitbucket.org/)
1. Create a [netlify](https://app.netlify.com/start) site from that repo
    - Netlify will create a unique url for your site that can be used right away, but I suggest setting up a custom domain that is short and easy to type/share
1. In the repo create a file named `_redirects`
1. Add some redirect lines (e.g. `/code https://example.com`) and commit the changes

You now have your very own URL shortener. Anytime you commit a change to the repo, Netlify will automatically rebuild the site and your updated short codes will be available to use within a few seconds.


Now let's make it even easier to add redirects

1. Clone the repo to your local machine
1. Make it a npm project (`npm init -y`)
1. Install `netlify-shortener')[https://github.com/kentcdodds/netlify-shortener] (`npm install netlify-shortener`)
1. Modify your `package.json` to include a "homepage" property for your url and to expose the script
  ```json
  "homepage": "https://example.com",
  "scripts": {
    "shorten": "netlify-shortener"
  }
  ```
1. Now you can run the script to add short codes to your redirect file
  ```sh
  npm run shorten https://yahoo.com # generates a random short code and adds it for you
  npm run shorten https://github.com gh # adds gh as a short URL for you
  ```

The final step in making it as easy as possible to add short codes is to add an alias that you can run from anywhere on your system. To that you can add the following to your `.zshrc` file
```
function shorten() {
  pushd ~/path/to/shortener/repo && npm run shorten "$1" "$2" && popd;
}
```
